import time
import serial
import sys

'''
    Note 1: The wattage control function works when the tube voltage is 40 kV or more. Settings in which the wattage will exceed 8 W (small focus mode) or 16 W (middle focus mode) or 39 W (large focus mode) cannot be made. In large focus mode, the maximum tube current is set to a fixed value of 300 uA when the tube voltage is 100 kV or less. When the tube voltage is changed, the tube current is automatically adjusted so as not to exceed the wattage limit because the tube voltage value takes priority. 
    Note 2: When the tube voltage is 39 kV or less, the tube current can be set up to a maximum of 200 uA (small focus mode), 300 uA (middle focus mode). However, this does not guarantee actual operation. For tube current setting versus tube voltage, see 15.8 X-ray tube voltage/current operation guaranteed range in 15. Appendix of this manual. 
    Note 3: When a CFS command (CFS 0 - 2) is sent to change the focus mode while xrays are being emitted, xray emission automatically stops.
    Note 4: "WUP" command - When the response to the STS command is STS 0 or STS 2, warm-up starts by sending a WUP command.
    Note 5: "TSF" command - When the response to the STS command is STS 2, a self-test starts by sending a TSF command. Upon sending the TSF command, X-rays automatically starts and then stops after about 90 seconds. The self-test continues for about 10 seconds after stopping X-ray emission. To interrupt the self -test before it is completed, send a XOF command. 
    Note 6: "RST" command - When the overload protection is activated, X-ray emission stops and the status return STS 4. The RST command resets the overload protection.
    
    
    Return 1: In order of priority:
        STS 5: xrays can't be emmited, circuit board defect, interlock open
        STS 4: overload protection is activated
        STS 1: warmup in progress
        STS 6: self test in progress
        STS 3: xrays being emmited
        STS 0: waiting for warmup
        STS 2: ready to emit xrays
    Return 2: returns preheating status:
        SPH 0: preheating complete
        SPH 1: preheating in progress
    Return 3: returns operation status, 7 paramaters returned, STS (status), SHV (output voltage), SCU (output current), latter 4 are reserved and return 0, i.e return will be "SAR 3 50 30 0 0 0 0"
    Return 4: returns status indicating that x-rays can't be ommited. 4 params returned. SER (hardware error), SIN (interlock), SPH (preheating) with the last being reserved as 0, i.e return will be "SNR 0 1 0 0"
    Return 5: returns the focus mode:
        SFC 0: small focus mode
        SFC 1: medium focus mode
        SFC 2: large focus mode
    Return 6: returns the warm up status
        SWE 0: warmup is complete
        SWE 1: warmup in progress
        SWE 2: warmup is ready but not started
    Return 7: returns the interlock status:
        SIN 1: interlock circuit is open
        SIN 0: interlock is closed
    Return 8: returns whether or not self check is complete
        ZTE 0: self test not run or not complete
        ZTE 1: self test complete
    Return 9: returns self test results
        ZTB 0: self test not run or NG (fail)
        ZTB 1: ok (pass)
        ZTB 2: self test in progress
    Return 10: returns self test detailed results, return format = ZTR a bb.b cc.c dd.d e f g hh ii where:
        a: cathode level
        bb.b: maximum value of input power supply voltage
        cc.c: minimum value of input power supply voltage
        dd.d: average value of input power supply voltage
        e: operation of control circuit 1
        f: operation of control circuit 2
        g: high voltage block
        hh: circuit board temp 1
        ii: circuit board temp 2
    Return 11: does coin battery need to be replaced?
        SBT 0: voltage nominal
        SBT 1: battery is low
        
        
    SER hardware error checks, in order of priority:
        SER 0: No errors found
        SER 3: control board error
        SER 4: control board error
        SER 200: cooling fan error
        SER 201: imput power supply voltage error
        SER 202: input power supply voltage error
        SER 204: control board error
        SER 207: control board error
        SER 208: input power supply voltage error
        SER 203: Control board error
        SER 209: Temperature alarm
        
        To clear errors, power cycle system.
        
    Work flow:
    
    warm up - WUP
    set xray focus mode
    set xray tube voltage
    set xray tube current
    xray on
    xray off
    
    
'''

class XrayControl():
    
    def __init__(self, port, baudrate, FocusMode, TubeVoltage, TubeCurrent):
        self.rs232 = serial.Serial(port = port, baudrate = baudrate, parity=serial.PARITY_ODD, stopbits=serial.STOPBITS_TWO, bytesize=serial.SEVENBITS)
        
        #External control command list - 3 commands here require parameters
        self.xray_start = "XON"
        self.xray_stop = "XOF"
        self.tube_voltage = "HIV" # requires parameter, in kv, see note 1 and note 2.
        self.tube_current = "CUR" # requires parameter, in kv, see note 1 and note 2.
        self.focus_mode = "CFS" # requires parameter, 0 = small, 1 = medium, 2 = large. see notes 1, 2 and 3.
        self.warm_up_test = "WUP" # see note 4
        self.self_test = "TSF" # see note 5
        self.overload_protection_reset = "RST" #see note 6
       
        #status commands - These commands will cause the Xray device to return some data
        self.status_check = "STS" #Return 1
        self.preheat_monitor = "SPH" #Return 2
        self.batch_status_check = "SAR" #Return 3
        self.not_ready_batch_check = "SNR" #Return 4
        self.output_tube_voltage = "SHV" #returns x ray tube voltage (Kv)
        self.output_tube_current = "SCU" #returns x ray tube current (uA)
        self.set_tube_voltage = "SPV" #returns the set tube voltage (Kv)
        self.set_tube_current = "SPC" #returns the set tube current (uA)
        self.set_tube_vc = "SVI" #returns the set tube current and voltage
        self.focus_mode_check = "SFC" # Return 5
        self.warm_up_pattern_check = "SWS" #returns the warmup mode and step
        self.warm_up_status_check = "SWE" #Return 6
        self.interlock_check = "SIN" #Return 7
        self.self_test_completion_check = "ZTE" #Return 8
        self.self_test_result = "ZTB" #Return 9
        self.self_test_detailed_result = "ZTR" #Return 10
        self.power_on_time_check = "STM" # retruns accumulated time that product has been on (hours)
        self.xray_emmissions_time_check = "SXT" #returns accumulated time xrays have been emmited during operation and warmup (hours)
        self.coin_battery_check = "SBT" #Return 11
        self.model_name_check = "SBT" #returns model name
        self.hardware_error_check = "SER" #see SER returns
        
        self.FocusMode = FocusMode
        self.TubeVoltage = TubeVoltage
        self.TubeCurrent = TubeCurrent
        
    def OpenSerial(self):
        self.rs232.open()
        self.rs232.isOpen()
        return
        
        
    def SendCommand(self, command):
        self.rs232.write(command + '\r')
        return
            
    def SendCommandAndParam(self, command, param):
        self.rs232.write(command + ' ' + param +'\r')
        return
            
    def SendCommandWaitForReturn(self, command):
        #print("sending command now")
        self.rs232.write(command + '\r')
        #print("waiting for return") 
        returned = self.rs232.read_until(b'\r')
        #print("retunred = ", returned)
        return returned
            
    def SendCommandAndParamWaitForReturn(self, command, param):
        self.rs232.write(command + ' ' + param + '\r')
        returned = self.rs232.read_until(b'\r')
        return returned
        
    def StatusCheck(self):
        hardware_status = self.SendCommandWaitForReturn(command = self.hardware_error_check)
        print(hardware_status)
        return
            
    def BasicMachineConfig(self):
            
        hardware_status = self.SendCommandWaitForReturn(command = self.hardware_error_check)
            
        if (hardware_status == 'SER 0\r'):
            print('hardware status ok \n')
            print(self.SendCommandAndParamWaitForReturn(command = self.focus_mode, param = self.FocusMode)) #set focus to small
            print(self.SendCommandAndParamWaitForReturn(command = self.tube_voltage, param = self.TubeVoltage)) #set tube voltage to 50 kv
            print(self.SendCommandAndParamWaitForReturn(command = self.tube_current, param = self.TubeCurrent)) #set tube current to 10 ua
            print("machine configured with start up paramaters.")
            return 1
                
        else:
            print("hardware status not ok, status = ", hardware_status)
            return sys.exit()
                
    def AdjustConfig(self,FocusMode,TubeVoltage,TubeCurrent):
        self.FocusMode = FocusMode
        self.TubeVoltage = TubeVoltage
        self.TubeCurrent = TubeCurrent
            
        hardware_status = self.SendCommandWaitForReturn(command = self.hardware_error_check)
            
        if (hardware_status == 'SER 0\r'):
            print('hardware status ok \n')
            print(self.SendCommandAndParamWaitForReturn(command = self.focus_mode, param = self.FocusMode)) #set focus to small
            print(self.SendCommandAndParamWaitForReturn(command = self.tube_voltage, param = self.TubeVoltage)) #set tube voltage to 50 kv
            print(self.SendCommandAndParamWaitForReturn(command = self.tube_current, param = self.TubeCurrent)) #set tube current to 10 ua
            print("machine configured with specified paramaters.")
            return 1
                
        else:
            print("hardware status not ok, status = ", hardware_status)
            return sys.exit()
            
                
            
    def WarmUp(self):
        STS_status = self.SendCommandWaitForReturn(command = self.status_check)
        print("STS staus = ", STS_status) 
        if (STS_status == 'STS 5\r'):
            return sys.exit()

        print(self.SendCommandWaitForReturn(command = self.warm_up_test)) #warm up tube
                
        while(self.SendCommandWaitForReturn(command = self.status_check) == 'STS 1\r'): #check status, looking for STS 2
            print("tube warming up")
            time.sleep(10)
            
        STS_status = self.SendCommandWaitForReturn(command = self.status_check)
        if (STS_status != 'STS 2\r'):# not ready to emit xrays
            print("error during warmup, status check returned ", STS_status)
            return sys.exit()
                        
        else:
            print("ready to emit x rays with chosen settings")
            return 1
                        
                        
        
                        
    def PulseXray(self,TimeOn):
        self.SendCommand(self.xray_start)
        print("Xray on!!")
        time.sleep(TimeOn)
        self.SendCommand(self.xray_stop)
        print("Xray off.")
        return
            
            
    def XrayOn(self):
        self.SendCommand(self.xray_start)
        print("Xray on!!")
        return
            
    def XrayOff(self):
        self.SendCommand(self.xray_stop)
        print("Xray off.")
        return
                   
        
            
            
def main():
    xray = XrayControl(port = '/dev/ttyACM1', baudrate = 9600, FocusMode = '0', TubeVoltage = '100', TubeCurrent = '50')
    xray.BasicMachineConfig()
    xray.AdjustConfig(FocusMode = '1', TubeVoltage = '70', TubeCurrent = '80')
    xray.WarmUp()
    xray.PulseXray(TimeOn = 1)
    xray.XrayOn()
    xray.XrayOff()
    
    


if __name__ == "__main__":
    main()

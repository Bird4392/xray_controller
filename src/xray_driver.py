#!/usr/bin/env python
import rospy
import controller
from std_srvs.srv import Trigger, TriggerResponse

class Services():
	def __init__(self,port, baud_rate, initial_focus_mode, initial_tube_voltage, initial_tube_current):
		self.xray_driver = controller.XrayControl(port, baud_rate, initial_focus_mode, initial_tube_voltage, initial_tube_current)
		print("driver initilised, passing initial config")
		self.xray_driver.BasicMachineConfig()
		self.warmup_service = rospy.Service('/warmup', Trigger, self.trigger_warmup)
		self.xray_on_service = rospy.Service('/xray_on', Trigger, self.trigger_xray_on)
		self.xray_off_service = rospy.Service('/xray_off', Trigger, self.trigger_xray_off)
		self.Xray_pulse_service = rospy.Service('/xray_pulse', Trigger, self.trigger_xray_pulse)

	def trigger_warmup(self,request):
		self.xray_driver.WarmUp()
		return TriggerResponse(success=True, message="warmup service")

	
	def trigger_xray_on(self,request):
		self.xray_driver.XrayOn()
		return TriggerResponse(success=True, message="xray on service")


	def trigger_xray_off(self,request):
		self.xray_driver.XrayOff()
		return TriggerResponse(success=True, message="xray off service")

	
	def trigger_xray_pulse(self,request):
		self.xray_driver.PulseXray(TimeOn = 1000)
		return TriggerResponse(success=True, message="xray pulse service")

    def status_check(self):
        self.xray_driver.StatusCheck()
        return



# Intializes everything
def start():
	baud_rate = rospy.get_param("/baud_rate")
	port = rospy.get_param('/port')
	initial_focus_mode = rospy.get_param('/initial_focus_mode')
	initial_tube_voltage = rospy.get_param('/initial_tube_voltage')
	initial_tube_current = rospy.get_param('/initial_tube_current')

	services = Services(port, baud_rate, initial_focus_mode, initial_tube_voltage, initial_tube_current)

	rospy.init_node('xray_driver')
    
    while(1):
        rospy.sleep(2.0)
        services.status_check()
        

if __name__ == '__main__':
	start()

